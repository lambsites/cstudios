; Make File Info
core = 7.x
api = 2

; Drupal Core
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "http://ftp.drupal.org/files/projects/drupal-7.39.zip"
; projects[drupal][version] = 7

; Installation Profile
projects[hydrant_profile][type] = "Profile"
projects[hydrant_profile][download][type] = "git"
projects[hydrant_profile][download][url] = "git@bitbucket.org:hydrant/hydrant-base-install-profile.git"
projects[hydrant_profile][directory_name] = "hydrant"

; drupal.org contrib modules
projects[administerusersbyrole][version] = 2.0-beta1
projects[administerusersbyrole][subdir] = contrib
projects[admin_theme][version] = 1.0
projects[admin_theme][subdir] = contrib
projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][subdir] = contrib
projects[backup_migrate][version] = 3.1
projects[backup_migrate][subdir] = contrib
projects[chart][version] = 1.1
projects[chart][subdir] = contrib
projects[ckeditor][version] = 1.16
projects[ckeditor][subdir] = contrib
projects[ckeditor_link][version] = 2.3
projects[ckeditor_link][subdir] = contrib
projects[ctools][version] = 1.9
projects[ctools][subdir] = contrib
projects[context][version] = 3.6
projects[context][subdir] = contrib
projects[custom_search][version] = 1.18
projects[custom_search][subdir] = contrib
projects[date][version] = 2.8
projects[date][subdir] = contrib
projects[devel][version] = 1.5
projects[devel][subdir] = contrib
projects[entity][version] = 1.6
projects[entity][subdir] = contrib
projects[entityreference][version] = 1.1
projects[entityreference][subdir] = contrib
projects[features][version] = 2.6
projects[features][subdir] = contrib
projects[field_formatter_settings][version] = 1.1
projects[field_formatter_settings][subdir] = contrib
projects[field_group][version] = 1.4
projects[field_group][subdir] = contrib
projects[field_word_boundary][version] = 1.0-beta3
projects[field_word_boundary][subdir] = contrib
projects[flood_unblock][version] = 1.4
projects[flood_unblock][subdir] = contrib
projects[globalredirect][version] = 1.5
projects[globalredirect][subdir] = contrib
projects[google_analytics][version] = 2.1
projects[google_analytics][subdir] = contrib
projects[google_analytics_reports][version] = 3.0-beta2
projects[google_analytics_reports][subdir] = contrib
projects[image_resize_filter][version] = 1.16
projects[image_resize_filter][subdir] = contrib
projects[imce][version] = 1.9
projects[imce][subdir] = contrib
projects[jquery_update][version] = 2.6
projects[jquery_update][subdir] = contrib
projects[less][version] = 3.0
projects[less][subdir] = contrib
projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib
projects[link][version] = 1.3
projects[link][subdir] = contrib
projects[maxlength][version] = 3.2
projects[maxlength][subdir] = patched

projects[maxlength][patch][] = https://www.drupal.org/files/issues/counter-flashing-and-failing-with-ckeditor-2267849-2.patch
projects[maxlength][patch][] = https://www.drupal.org/files/issues/maxlength-enforce-and-truncate-summary-2382853-2.patch

projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = contrib
projects[menu_admin_per_menu][version] = 1.1
projects[menu_admin_per_menu][subdir] = contrib
projects[menu_block][version] = 2.7
projects[menu_block][subdir] = contrib
projects[menu_position][version] = 1.1
projects[menu_position][subdir] = contrib
projects[metatag][version] = 1.7
projects[metatag][subdir] = contrib
projects[module_filter][version] = 2.0
projects[module_filter][subdir] = contrib
projects[navbar][version] = 1.6
projects[navbar][subdir] = contrib
projects[oauth][version] = 3.2
projects[oauth][subdir] = contrib
projects[override_node_options][version] = 1.13
projects[override_node_options][subdir] = contrib
projects[password_policy][version] = 1.12
projects[password_policy][subdir] = contrib
projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib
projects[reroute_email][version] = 1.2
projects[reroute_email][subdir] = contrib
projects[redirect][version] = 1.0-rc3
projects[redirect][subdir] = contrib
projects[role_delegation][version] = 1.1
projects[role_delegation][subdir] = contrib
projects[search_autocomplete][version] = 4.6
projects[search_autocomplete][subdir] = contrib
projects[site_verify][version] = 1.1
projects[site_verify][subdir] = contrib
projects[strongarm][version] = 2.0
projects[strongarm][subdir] = contrib
projects[styleguide][version] = 1.1
projects[styleguide][subdir] = contrib
projects[transliteration][version] = 3.2
projects[transliteration][subdir] = contrib
projects[token][version] = 1.6
projects[token][subdir] = contrib
projects[varnish][version] = 1.0-beta2
projects[varnish][subdir] = contrib
projects[video_embed_field][version] = 2.0-beta8
projects[video_embed_field][subdir] = contrib
projects[views][version] = 3.11
projects[views][subdir] = contrib
projects[views_bulk_operations][version] = 3.3
projects[views_bulk_operations][subdir] = contrib
projects[vppr][version] = 1.0
projects[vppr][subdir] = contrib
projects[webform][version] = 4.10
projects[webform][subdir] = contrib
projects[xmlsitemap][version] = 2.2
projects[xmlsitemap][subdir] = contrib

; Theme project
projects[bootstrap][version] = 3.0


; Libraries

; CKEditor
; libraries[ckeditor][download][type] = "get"
; libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.5/ckeditor_4.4.5_full.tar.gz"
; libraries[ckeditor][destination] = "libraries"

; Less
libraries[lessphp][download][type] = "get"
libraries[lessphp][download][url] = "https://github.com/oyejorge/less.php/archive/v1.7.0.2.tar.gz"
libraries[lessphp][destination] = "libraries"

; Backbone
libraries[backbone][download][type] = "get"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/master.zip"
libraries[backbone][destination] = "libraries"

; Modernizr
libraries[modernizr][download][type] = "git"
libraries[modernizr][download][url] = "git@bitbucket.org:hydrant/modernizr.git"
libraries[modernizr][destination] = "libraries"

; Underscore
libraries[underscore][download][type] = "get"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/master.zip"
libraries[underscore][destination] = "libraries"

; Hydrant "ckeditor" build.
; (Currently the bespoke part is the inclusion of image2 [Enhanced image])
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "git@bitbucket.org:hydrant/ckeditor.git"
libraries[ckeditor][options][working-copy] = "false"

; Hydrant "features" modules
projects[hydrant_features][type] = "module"
projects[hydrant_features][download][type] = "git"
projects[hydrant_features][download][url] = "git@bitbucket.org:hydrant/hydrant-base-features.git"
projects[hydrant_features][options][working-copy] = "false"
projects[hydrant_features][subdir] = features

; Hydrant admin admin_theme
projects[hydrant_admin][type] = "theme"
projects[hydrant_admin][download][type] = "git"
projects[hydrant_admin][download][url] = "git@bitbucket.org:hydrant/hydrant-admin-theme.git"
projects[hydrant_admin][options][working-copy] = "false"

; Hydrant site settings
projects[hydrant_settings][type] = "module"
projects[hydrant_settings][download][type] = "git"
projects[hydrant_settings][download][url] = "git@bitbucket.org:hydrant/hydrant_settings.git"
projects[hydrant_settings][options][working-copy] = "false"
projects[hydrant_settings][subdir] = custom

; Hydrant site monitor module
projects[hydrant_monitor][type] = "module"
projects[hydrant_monitor][download][type] = "git"
projects[hydrant_monitor][download][url] = "git@bitbucket.org:hydrant/hydrant-monitor.git"
projects[hydrant_monitor][options][working-copy] = "false"
projects[hydrant_monitor][subdir] = custom

; Hydrant Bootstrap starter theme
projects[hydrant_bootstrap][type] = "theme"
projects[hydrant_bootstrap][download][type] = "git"
projects[hydrant_bootstrap][download][url] = "git@bitbucket.org:hydrant/hydrant_bootstrap.git"
projects[hydrant_bootstrap][options][working-copy] = "false"

; Hydrant styleguide extensions
projects[hydrant_styleguide_additions][type] = "module"
projects[hydrant_styleguide_additions][download][type] = "git"
projects[hydrant_styleguide_additions][download][url] = "git@bitbucket.org:hydrant/hydrant-style-guide-extensions-for-drupal.git"
projects[hydrant_styleguide_additions][options][working-copy] = "false"
projects[hydrant_styleguide_additions][subdir] = custom

; Hydrant CacheFlusher
projects[cacheflusher][type] = "module"
projects[cacheflusher][download][type] = "git"
projects[cacheflusher][download][url] = "git@bitbucket.org:hydrant/cacheflusher-module.git"
projects[cacheflusher][options][working-copy] = "false"
projects[cacheflusher][subdir] = custom

; Hydrant Start
; projects[hydrant_start][type] = "module"
; projects[hydrant_start][download][type] = "git"
; projects[hydrant_start][download][url] = "git@bitbucket.org:hydrant/hydrant-start.git"
; projects[hydrant_start][options][working-copy] = "false"
; projects[hydrant_start][subdir] = custom

; Schema Breadcrumb
projects[schema_breadcrumb][type] = "module"
projects[schema_breadcrumb][download][type] = "git"
projects[schema_breadcrumb][download][url] = "git@bitbucket.org:hydrant/schema-breadcrumb.git"
projects[schema_breadcrumb][options][working-copy] = "false"
projects[schema_breadcrumb][subdir] = custom
