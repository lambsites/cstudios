<?php

/**
* Implement hook_form_install_configure_form_alter().
*
* Set some default site information to save entering it every time
*/

function hydrant_form_install_configure_form_alter(&$form, $form_state) {

  // Pre-populate the site email address.
  $form['site_information']['site_mail']['#default_value'] = 'tracking@hydrant.co.uk';

  // Account information defaults
  $form['admin_account']['account']['name']['#default_value'] = 'hyadmin';
  $form['admin_account']['account']['mail']['#default_value'] = 'tracking@hydrant.co.uk';

  // Date/time settings
  $form['server_settings']['site_default_country']['#default_value'] = 'GB';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/London';

  // Unset the timezone detect stuff
  unset($form['server_settings']['date_default_timezone']['#attributes']['class']);

  // Only check for updates, no need for email notifications
  $form['update_notifications']['update_status_module']['#default_value'] = array(0);

}