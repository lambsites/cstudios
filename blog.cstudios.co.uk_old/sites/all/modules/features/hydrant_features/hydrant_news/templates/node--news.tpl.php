<?php if ($teaser) { ?>


  <div itemscope itemtype="http://schema.org/Article">

    <?php if (!empty($content['field_news_img'])) { ?>
      <div class="col-xs-12 col-sm-3">
        <?php print render($content['field_news_img']); ?>
      </div>
    <?php } ?>

    <div class="listing-summary col-xs-12<?php if (!empty($content['field_news_img'])) { print ' col-sm-9'; } ; ?>">

      <h3 class="title" itemprop="headline">
        <a href="<?php print $node_url; ?>" itemprop="url">
          <?php print $title; ?>
        </a>
      </h3>

      <div class="date" itemprop="datePublished">
        <?php print format_date($node->created, 'long'); ?>
      </div>

      <div itemprop="description">
        <?php print render($content['body']); ?>
      </div>

      <?php
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      ?>

    </div>

  </div>


<?php } else { ?>


  <div itemscope itemtype="http://schema.org/Article">

    <div class="listing-content">

      <?php if (!empty($content['field_news_img'])) { ?>
        <?php print render($content['field_news_img']); ?>
      <?php } ?>

      <div class="date" itemprop="datePublished">
        <?php print format_date($node->created, 'long'); ?>
      </div>

      <div itemprop="articleBody">
        <?php print render($content['body']); ?>
      </div>

      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      ?>

    </div>

  </div>


<?php } ?>



