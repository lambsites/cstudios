#Hydrant News Feature

Includes functionality to create and manage news articles and a series of “views” to list them. In more detail, this consists of:

## A "News" content type

With the following fields:

* Basic content vertical tab containing all fields
* Title
* Body
* Image
* Attachments.

and the following settings:

* Preview, promote frontpage and date & author information disabled.
* Revisions enabled
* No menu settings
* XML sitemap inclusion
* A path alias of “news/[node:title]”

##A “view” titled "News" with the following

* A page created with path “news”, showing the latest news articles, most recent first, with pagination, 10 articles per page and a menu link titled “News”, which appears in the main menu.
* An "Archive" block showing number of news articles per month.
* A "Recent news" block showing the 5 most recent articles.

## A menu-position rule to show under the “News” menu item, for

* “News nodes”
* Any paths which contain “news” as the first argument in the path (Such as “Archive pages” from the “News” view).

## Sets basic permissions to:

* Create articles
* Edit articles
* Delete article
* Publish articles.

## A template file for articles, with the filename : node--news.tpl.php

* Markup for both teaser and full display.
* Including Microdata attributes for schema.org.

When the feature is uninstalled, the view, menu position and block assignment will be removed. The content type will also no longer be available, however, any nodes which may have been created will not be deleted.


## Change log

2015-14-01

* Initial release