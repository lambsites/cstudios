<?php
/**
 * @file
 * hydrant_existing_content.features.inc
 */

/**
 * Implements hook_views_api().
 */
function hydrant_existing_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
