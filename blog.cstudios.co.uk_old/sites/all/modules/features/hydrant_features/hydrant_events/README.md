#Hydrant Event Feature

Includes functionality to create and manage events and a series of “views” to list them. In more detail, this consists of:

## An "Event" content type

With the following fields:

* Basic content vertical tab containing all fields
* Title
* Date (Start and End)
* Body
* Image
* Attachments.

and the following settings:

* Preview, promote frontpage and date & author information disabled.
* Revisions enabled
* No menu settings
* XML sitemap inclusion
* A path alias of “events/[node:title]”

##A “view” titled "Events" with the following

* A page created with path “events”, showing approaching events, order by start date descending, with pagination, 10 articles per page and a menu link titled “Events”, which appears in the main menu.
* A "By month" block showing events which occur in a particular month.
* A "Upcoming events" block showing the 5 next events to happen.

## A menu-position rule to show under the “Events” menu item, for

* “Event nodes”
* Any paths which contain “events” as the first argument in the path (Such as “Monthly pages” from the “Events” view).

## Sets basic permissions to:

* Create events
* Edit events
* Delete events
* Publish events.
* Make events sticky

## A template file for events, with the filename : node--event.tpl.php

* Markup for both teaser and full display.
* Including Microdata attributes for schema.org.
* Once an events end date is in the past, a variable will display a string flagging that the event has occurred. This can be overridden using string overrides.

When the feature is uninstalled, the view, menu position and block assignment will be removed. The content type will also no longer be available, however, any nodes which may have been created will not be deleted.


## To do
* Schema.org requires that location (physical address) information is included. We are currently proposing using the sites address, from a "site settings" module (still to be built), is used, or to have location fields on the event itself.


## Change log

2015-14-01

* Initial release