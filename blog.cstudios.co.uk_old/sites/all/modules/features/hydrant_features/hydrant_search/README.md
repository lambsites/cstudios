#Hydrant Search Feature

This is a very basic feature, which enables the Drupal Core search module and assigns it to the right sidebar. At the same time, cron is run to index any existing content.

The module additionally, requires the "Custom search" contributed module, which offers additional configuration, such as choosing which entities and entity types (such as content types and taxonomy vocabularies) are included in search results. Some of the configuration in the Custom search module options are set when the feature is enabled.

There are additional features, which extend the search (such as "Search autocomplete").


## Change log

2015-23-02

* Initial release