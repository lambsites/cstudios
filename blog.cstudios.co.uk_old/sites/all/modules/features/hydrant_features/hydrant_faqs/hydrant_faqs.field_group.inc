<?php
/**
 * @file
 * hydrant_faqs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function hydrant_faqs_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basics|node|frequently_asked_question|form';
  $field_group->group_name = 'group_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'faq';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic settings',
    'weight' => '0',
    'children' => array(
      0 => 'field_faq_question',
      1 => 'field_faq_answer',
      2 => 'field_faq_categories',
      3 => 'field_faq_weight',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basics field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basics|node|frequently_asked_question|form'] = $field_group;

  return $export;
}
