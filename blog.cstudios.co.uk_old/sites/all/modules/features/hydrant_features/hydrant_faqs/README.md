#Hydrant FAQs Feature

Includes functionality to create and manage frequently asked questions and a series of “views” to list them. In more detail, this consists of:

## An "FAQ" content type

With the following fields:

* Basic content vertical tab containing all fields
* Question (This populates the title using the auto nodetitle module)
* Answer
* Category (A taxonomy vocabulary specifically for faqs)
* Order (An integer select list, so that questions can be asnwered)

and the following settings:

* Preview, promote frontpage and date & author information disabled.
* Revisions enabled
* No menu settings
* XML sitemap exclusion
* A path alias of “frequently-asked-questions/[node:title]”

##A “view” titled "FAQ's" with the following

* A page created with path “frequently-asked-questions”, ordered by the "order" field, with no pagination - all faq's are displayed on the same page, and a menu link titled “FAQ's”, which appears in the main menu. Post date is used as a secondary sort, if the order values are the same.

## A detail redirect

* When attemping to view the node path of an faq, this will be re-written to go to the FAQ's view.

## Sets basic permissions to:

* Create faqs
* Edit faqs
* Delete faqs
* Publish faqs.

## A template file for faqs, with the filename : node--faq.tpl.php

* Markup for both teaser and full display are the same.
* The template file uses Bootstraps "accordion" markup, so that clicking on the question will reveal the answer.

When the feature is uninstalled, the view will be removed. The content type will also no longer be available, however, any nodes which may have been created will not be deleted.

Additonally there are template files, named field--field-faq-question.tpl.php and field--field-faq-answer.tpl.php, which are used to clear default field markup, which would interfere with the Bootstrap markup for the accordion effect.


## Change log

2015-02-23

* Initial release