<div class="list-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">


    <?php // Question ?>
    <div class="panel-heading" role="tab" id="heading-<?php print $node->nid; ?>">

      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php print $node->nid; ?>" aria-expanded="false" aria-controls="collapse-<?php print $node->nid; ?>">
          <?php print render($content['field_faq_question']); ?>
        </a>
      </h4>

    </div>


    <?php // Answer ?>
    <div id="collapse-<?php print $node->nid; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php print $node->nid; ?>">

      <div class="panel-body">
        <?php print render($content['field_faq_answer']); ?>
      </div>

    </div>


  </div>

</div>