<?php
/**
 * @file
 * hydrant_faqs.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function hydrant_faqs_taxonomy_default_vocabularies() {
  return array(
    'faq_categories' => array(
      'name' => 'Frequently asked questions categories',
      'machine_name' => 'faq_categories',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
