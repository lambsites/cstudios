<?php
/**
 * @file
 * hydrant_search_autocomplete.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hydrant_search_autocomplete_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer Search Autocomplete'.
  $permissions['administer Search Autocomplete'] = array(
    'name' => 'administer Search Autocomplete',
    'roles' => array(),
    'module' => 'search_autocomplete',
  );

  // Exported permission: 'use Search Autocomplete'.
  $permissions['use Search Autocomplete'] = array(
    'name' => 'use Search Autocomplete',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_autocomplete',
  );

  return $permissions;
}
