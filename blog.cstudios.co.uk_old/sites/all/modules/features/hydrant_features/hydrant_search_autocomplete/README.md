#Hydrant Search Autocomplete Feature

This feature extends the "Hydrant Search" module, using the contrib module "Search Autocomplete", to suggest content as you type in the search field of both the Drupal Core search block and Drupal Core search page.

Suggestions are based on keywords in the node title.

This does NOT use the Drupal Core autocomplete field, commonly seen for tagging with taxonomy. Search autocomplete returns suggestions far quicker and integrates with views for the suggestion results, making it very flexible in configuration.


## Change log

2015-23-02

* Initial release