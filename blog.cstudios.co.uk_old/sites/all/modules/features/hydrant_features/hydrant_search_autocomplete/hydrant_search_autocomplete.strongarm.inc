<?php
/**
 * @file
 * hydrant_search_autocomplete.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function hydrant_search_autocomplete_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sa_admin_helper';
  $strongarm->value = 0;
  $export['sa_admin_helper'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sa_translite';
  $strongarm->value = 1;
  $export['sa_translite'] = $strongarm;

  return $export;
}
