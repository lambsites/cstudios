<?php
/**
 * @file
 * hydrant_blog.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function hydrant_blog_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basics|node|blog|form';
  $field_group->group_name = 'group_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic settings',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_attachments',
      2 => 'field_blog_img',
      3 => 'field_category',
      4 => 'field_tags',
      5 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basics field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basics|node|blog|form'] = $field_group;

  return $export;
}
