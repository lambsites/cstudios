# Hydrant Features

This project provides a group of features that will enhance your sites functionality, below is a list of currently available features.

* **Hydrant Admin Toolbar**, updates the admin toolbar on the front of your site so it matches the admin theme colours.
* **Hydrant Backup Migrate**, sets up a daily backup schedule that will keep 3 backups at a time.
* **Hydrant Basic Content**, will populate your site with some basic pages and a homepage.
* **Hydrant Embedded Node View Mode**, creates a demo view_mode for node displays.
* **Hydrant Events**, this creates a Events content type with correct permissions.
* **Hydrant Existing Content**, creates a new content display view for browsing you sites content.
* **Hydrant News**, creates a basic News content type with permissions.
* **Hydrant Sample News Content**, will populate the News content type with content, please install Hydrant News first!

## Changelog

2015-16-01

* Initial release