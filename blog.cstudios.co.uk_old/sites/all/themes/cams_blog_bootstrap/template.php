<?php

function cams_blog_bootstrap_breadcrumb($variables) {

  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {

    // If schema breacrumb module exists, use it
    if (module_exists('schema_breadcrumb')) {

      return theme('schema_breadcrumb', array(
          'attributes' => array(
            'class' => array('breadcrumb'),
          ),
          'items' => $breadcrumb,
          'type' => 'ol',
        )
      );

    // Otherwise, use a standard numbered list
    } else {

      return theme('item_list', array(
          'attributes' => array(
          'class' => array('breadcrumb'),
          ),
          'items' => $breadcrumb,
          'type' => 'ol',
        )
      );

    }

  }

}


function cams_blog_bootstrap_js_alter(&$javascript) {

  // Collect the scripts we want in to remain in the header scope.
  $header_scripts = array(
    //'sites/all/themes/themename/js/modernizr.js',
  );

  // Change the default scope of all other scripts to footer.
  // We assume if the script is scoped to header it was done so by default.
  foreach ($javascript as $key => &$script) {
    if ($script['scope'] == 'header' && !in_array($script['data'], $header_scripts)) {
      $script['scope'] = 'footer';
    }
  }

}


function cams_blog_bootstrap_image($variables) {

  // Remove image width and height attributes
  $attributes = $variables['attributes'];

  $attributes['src'] = file_create_url($variables['path']);

  foreach (array('alt', 'title') as $key) {

	if (isset($variables[$key])) {
	  $attributes[$key] = $variables[$key];
	}

  }

  return '<img' . drupal_attributes($attributes) . ' />';

}



/**
 * Implements hook_preprocess_html().
 *
 * @see html.tpl.php
 */
function cams_blog_bootstrap_preprocess_html(&$variables) {

  //** Meta tags **//

  // Viewport
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    )
  );

  // HandheldFriendly
  $meta_handheld = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'HandheldFriendly',
      'content' => 'True',
    )
  );

  // Mobile optimised
  $meta_mobileoptimized = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'MobileOptimized',
      'content' => '320',
    )
  );

  // Touch icon high
  $appleIconHigh = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/h/apple-touch-icon.png',
      'rel' => 'apple-touch-icon-precomposed',
      'sizes' => '114x114'
    ),
  );

  // Touch icon medium
  $appleIconMedium = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/m/apple-touch-icon.png',
      'rel' => 'apple-touch-icon-precomposed',
      'sizes' => '72x72'
    ),
  );

  // Touch icon low
  $appleIconLow = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/l/apple-touch-icon-precomposed.png',
      'rel' => 'apple-touch-icon-precomposed'
    ),
  );

  // Touch icon
  $appleIconShortcut = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/l/apple-touch-icon.png',
      'rel' => 'shortcut icon'
    ),
  );


  drupal_add_html_head($meta_viewport, 'viewport');
  drupal_add_html_head($meta_handheld, 'handheld');
  drupal_add_html_head($meta_mobileoptimized, 'mobileoptimized');

  drupal_add_html_head($appleIconHigh, 'appleIconHigh');
  drupal_add_html_head($appleIconMedium, 'appleIconMedium');
  drupal_add_html_head($appleIconLow, 'appleIconLow');
  drupal_add_html_head($appleIconShortcut, 'appleIconShortcut');

}


/**
 * Implements hook_preprocess_page().
 *
 * @see page.tpl.php
 */
function cams_blog_bootstrap_preprocess_page(&$variables) {

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-6"';
  } else if (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-9"';
  } else {
    $variables['content_column_class'] = ' class="col-sm-12"';
  }

  // Primary nav.
  $variables['primary_nav'] = FALSE;

  if ($variables['main_menu']) {
    // Build links.
    $variables['primary_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }

  // Secondary nav.
  $variables['secondary_nav'] = FALSE;
  if ($variables['secondary_menu']) {
    // Build links.
    $variables['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source', 'user-menu'));
    // Provide default theme wrapper function.
    $variables['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }

  $variables['navbar_classes_array'] = array('navbar');

  if (theme_get_setting('bootstrap_navbar_position') !== '') {
    $variables['navbar_classes_array'][] = 'navbar-' . theme_get_setting('bootstrap_navbar_position');
  } else {
    $variables['navbar_classes_array'][] = 'container-fluid';
  }

  if (theme_get_setting('bootstrap_navbar_inverse')) {
    $variables['navbar_classes_array'][] = 'navbar-inverse';
  } else {
    $variables['navbar_classes_array'][] = 'navbar-default';
  }

}


// Classes on main menu ul
function cams_blog_bootstrap_menu_tree__main_menu($variables){
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}

// Classes on user menu ul
function cams_blog_bootstrap_menu_tree__secondary($variables){
  return '<ul class="menu nav navbar-nav navbar-right">' . $variables['tree'] . '</ul>';
}

// Classes on footer menu ul
function cams_blog_bootstrap_menu_tree__footer_menu($variables){
  return '<ul class="menu nav nav-pills">' . $variables['tree'] . '</ul>';
}


/**
 * Replacement for theme_webform_element().
 */
function cams_blog_bootstrap_webform_element($variables) {

  $variables['element'] += array(
    '#title_display' => 'before',
  );

  $element = $variables['element'];

  if (isset($element['#format']) && $element['#format'] == 'html') {
    $type = 'display';
  }
  else {
    $type = (isset($element['#type']) && !in_array($element['#type'], array('markup', 'textfield', 'webform_email', 'webform_number'))) ? $element['#type'] : $element['#webform_component']['type'];
  }

  $nested_level = $element['#parents'][0] == 'submitted' ? 1 : 0;

  $parents = str_replace('_', '-', implode('--', array_slice($element['#parents'], $nested_level)));

  $wrapper_classes = array(
   'form-group',
   'webform-component',
   'webform-component--'.$parents,
  );

  if (isset($element['#container_class'])) {
    $wrapper_classes[] = $element['#container_class'];
  }

  if (isset($element['#title_display']) && strcmp($element['#title_display'], 'inline') === 0) {
    $wrapper_classes[] = 'webform-container-inline';
  }

  $output = '<div class="' . implode(' ', $wrapper_classes) . '" id="webform-component-' . $parents . '">' . "\n";

  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }

  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . _webform_filter_xss($element['#field_prefix']) . '</span> ' : '';

  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . _webform_filter_xss($element['#field_suffix']) . '</span>' : '';

  switch ($element['#title_display']) {
    case 'inline':
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;
    case 'none':
    case 'attribute':
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= ' <p class="help-block">' . $element['#description'] . "</p>\n";
  }

  $output .= "</div>\n";

  return $output;

}


function cams_blog_bootstrap_preprocess_image(&$variables) {

  // Remove a class to all images for responsive compatibility
  if (!empty($variables['attributes']['class'])) {
    foreach ($variables['attributes']['class'] as $key => $value) {
     if ($value == "img-responsive") {
        unset($variables['attributes']['class'][$key]);
     }
    }
  }

}


function cams_blog_bootstrap_preprocess_date_display_range(&$variables) {

  // Add schema attributes to all date fields
  // This would ideally be in the hydrant_events feature,
  // but needs to run at theme level.
  $dates = $variables['dates'];

  $variables['attributes_start']['itemprop'] = 'startDate';
  $variables['attributes_start']['content'] = $dates['value']['formatted_iso'];

  $variables['attributes_end']['itemprop'] = 'endDate';
  $variables['attributes_end']['content'] = $dates['value2']['formatted_iso'];

}


/**
 * Implements hook_element_info_alter().
 * (See bootstrap theme - theme/process.inc)
 */
function cams_blog_bootstrap_element_info_alter(&$elements) {
  foreach ($elements as &$element) {
    // Process input elements.
    if (!empty($element['#input'])) {
      $element['#process'][] = '_cams_blog_bootstrap_process_input';
    }
  }
}



/**
 * Process input elements.
 * (See bootstrap theme - theme/process.inc)
 */
function _cams_blog_bootstrap_process_input(&$element, &$form_state) {
  // Only add the "form-control" class for specific element input types.
  $types = array(
    'webform_email',
  );
  if (!empty($element['#type']) && (in_array($element['#type'], $types) || ($element['#type'] === 'file' && empty($element['#managed_file'])))) {
    $element['#attributes']['class'][] = 'form-control';
  }
  return $element;
}
