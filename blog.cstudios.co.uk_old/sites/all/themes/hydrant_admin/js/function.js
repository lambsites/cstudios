(function($){

  $(function(){

    //Back to the top link animates instead of quick jump
	  $(".scroll").click(function(event){
		  event.preventDefault();
		  $('html,body').animate({scrollTop:$(this.hash).offset().top}, 700);
	  });

  });

})(jQuery);