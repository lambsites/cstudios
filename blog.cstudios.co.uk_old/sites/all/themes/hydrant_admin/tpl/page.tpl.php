<div id="branding">

  <div id="meta">
    <p id="visitme"><a href="<?php print base_path(); ?>" target="_blank" title="<?php print t('View your website in a new window'); ?>"><?php print t('Visit your site'); ?></a></p>
  </div>

  <?php print $breadcrumb; ?>

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <h1><?php print $title; ?></h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php print render($primary_local_tasks); ?>

</div><!--branding-->

<div id="page">

  <div id="content">

    <?php if ($secondary_local_tasks): ?>
      <div class="tabs-secondary clearfix"><ul class="tabs secondary"><?php print render($secondary_local_tasks); ?></ul></div>
    <?php endif; ?>

    <?php if ($messages): ?>
      <div id="console"><?php print $messages; ?></div>
    <?php endif; ?>

    <?php if ($page['help']): ?>
      <div id="help">
        <?php print render($page['help']); ?>
      </div><!--help-->
    <?php endif; ?>

    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

    <?php print render($page['content']); ?>

  </div><!--content-->

  <div id="siteinfo">
    <div class="vcard">
      <div class="adr">
        <div class="tel">0845 862 1111</div>
      </div>
      <div class="mail"><a href="mailto:support@hydrant.co.uk">support@hydrant.co.uk</a></div>
    </div>
	  <div id="top"><a href="#scrollto" class="scroll"><?php print t('Back to top'); ?></a></div>
    <p id="hydrant"><a href="http://www.hydrant.co.uk" title="Powered by Hydrant">Powered by Hydrant</a></p>
  </div>

</div><!--page-->