<?php include 'template/html-head.php'; ?>
</head>
<body>
    <div class="<?=$c->getPageWrapperClass()?>">
        <?php include 'template/header.php'; ?>
        <div class="mainPage">
            <div class="container">
                <div class="containerOuter">
                    <div class="containerBorder">
                        <?php include 'template/nav.php'; ?>
                        <h1 class="center">
                            <?php
                                $a = new Area('PageTitle');
                                $a->display($c);
                            ?>
                        </h1>
                        <div class="pageSection BGpastelBluePurple">
                            <?php
                                $a = new Area('PageContent');
                                $a->display($c);
                            ?>
                        </div>
                        <?php include 'template/footer.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?=Loader::element('footer_required');?>
</body>
</html>