<?php include 'template/html-head.php'; ?>
</head>
<body>
    <div class="<?=$c->getPageWrapperClass()?>">
        <?php include 'template/header.php'; ?>
        <?php include 'template/nav.php'; ?>
        <div class="mainPage">
            <div class="container">
                <h1 class="center">
                    <?php
                        $a = new Area('PageTitle');
                        $a->display($c);
                    ?>
                </h1>
                <div class="pageSection BGpastelGreenBlue">
                    <?php
                        $a = new Area('PageContent1');
                        $a->display($c);
                    ?>
                </div>
                <div class="pageSection BGpastelBluePurple">
                    <?php
                        $a = new Area('PageContent2');
                        $a->display($c);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?=Loader::element('footer_required');?>
</body>
</html>