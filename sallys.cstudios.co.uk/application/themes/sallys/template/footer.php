    <div class="footer">
        <div class="container">
            <div class="footHoriz center">
                <div class="vAlignMid col-4">
                    <img class="footerLogo" src="<?=$view->getThemePath()?>/images/logo.png" alt="Sallys Logo">
                </div>
                
                <div class="vAlignMid col-4x3 footDetailsContainer">
                    <p class="footDetails">WARWICK BRIDGE<br /><span class="phone">01228 317071</span></p>
                </div>
            </div>
            <div class="footHoriz center">
                <p style="font-size: 20px;">The Post Office, Warwick Bridge CA4 8RN Carlisle, Cumbria<br />sallyswarwickbridge@gmail.com</p>
            </div>
        </div>
    </div>