    <div class="navWrapper section">
        <div class="container">
            <div class="topNav center">
                <?php
                $bt_nav = BlockType::getByHandle('autonav');
                $bt_nav->controller->displayPages = 'top';
                $bt_nav->controller->orderBy = 'display_asc';
                $bt_nav->controller->displaySubPages = 'none';
                $bt_nav->render('view');
                ?>
                <ul class="nav">
                    
                </ul>
            </div>
        </div>
    </div>