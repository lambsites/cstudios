<?php

return array(
    'default-connection' => 'concrete',
    'connections' => array(
        'concrete' => array(
            'driver' => 'c5_pdo_mysql',
            'server' => 'localhost',
            'database' => 'soft_sallys',
            'username' => 'soft_sallys',
            'password' => 's@llyp@ss321',
            'charset' => 'utf8'
        )
    )
);
