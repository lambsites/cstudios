<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2015-05-30T23:10:01+00:00
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'site' => 'Sally\'s',
    'version_installed' => '5.7.2.1',
    'misc' => array(
        'access_entity_updated' => 1427051749,
        'latest_version' => '5.6.3.3',
        'seen_introduction' => true,
        'do_page_reindex_check' => false
    )
);
