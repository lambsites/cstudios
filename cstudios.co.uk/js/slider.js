var slideCount = 0;

function getSlideImageHeight() {
    'use strict';
    
    return document.getElementById("firstSlideImage").clientHeight;
}
function setSlideHeight() {
    'use strict';
    
    document.getElementById("imageSlideArea").style.height = getSlideImageHeight() + "px";
}

function getImageCount() {
    'use strict';
    
    return $("#imageSlideArea img").length;
}

function slide(number) {
    'use strict';
    
    if (number == null) {
        var slideBy = getSlideImageHeight() * slideCount + "px";
    } else {
        var slideBy = getSlideImageHeight() * number + "px";
    }
    
    document.getElementById("firstSlideImage").style.marginTop = "-" + slideBy;
    
    if (slideCount < getImageCount()) {
        slideCount++;
    } else {
        slideCount = 0;
    }
}

function slidePause(number) {
    'use strict';
    
    slide(number);
    
}

window.onresize = function (event) {
    'use strict';
    
    setSlideHeight();
    if (slideCount == 0) {
        slide(slideCount);
    } else {
        slide(slideCount-1);
    }
    
};

$( document ).ready(function() {
    setSlideHeight();
});

window.setInterval( function () {
    
    slide();
    
}, 5000);