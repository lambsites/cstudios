<?php include_once 'php/htmlHead.php';?>
    </head>
    
    
    
    <body>
        <?php include_once 'templates/header.php';?>
        
        
        <div class="wrapper">
            <div class="pageContent">
                <div class="container">
                    <div class="centered">
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle">Contact Form</h1><hr class="titleHR" />
                        <form class="contactForm" method="post" action="php/contactScript.php">
                            <input type="text" name="ContactName" placeholder="Your Name">
                            <input type="text" name="ContactEmail" placeholder="Your Email">
                            <input type="text" name="ContactPhone" placeholder="Your Phone Number">
                            <textarea name="ContactMessage" cols="30" rows="10" placeholder="Message"></textarea>
                            <input type="submit" value="Send">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        
        <?php include_once 'templates/footer.php';?>
    </body>
    
</html>