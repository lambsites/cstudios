<?php include_once 'php/htmlHead.php';?>
    </head>
    
    
    
    <body>
        <?php include_once 'templates/header.php';?>
        
        
        <div class="wrapper">
            <div class="pageContent">
                <div class="container">
                    <div class="centered">
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle"><img src="images/team/cameron.jpg" alt="" class="teamProfile"><br />Cameron Lamb</h1><hr class="titleHR" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil beatae autem blanditiis fugiat perspiciatis, ea rem, provident vel facere quibusdam ipsa eum impedit placeat, exercitationem facilis! Aliquid blanditiis iste magni!</p>
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle"><img src="images/team/cameron.jpg" alt="" class="teamProfile"><br />Jonathon Russell</h1><hr class="titleHR" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil beatae autem blanditiis fugiat perspiciatis, ea rem, provident vel facere quibusdam ipsa eum impedit placeat, exercitationem facilis! Aliquid blanditiis iste magni!</p>
                    </div>
                </div>
            </div>
        </div>
        
        
        <?php include_once 'templates/footer.php';?>
    </body>
    
</html>