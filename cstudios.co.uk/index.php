<?php include_once 'php/htmlHead.php';?>
    </head>
    
    
    
    <body>
        <?php include_once 'templates/header.php';?>
        <?php include_once 'templates/slider.php';?>
        
        
        <div class="wrapper">
            <div class="pageContent">
                <div class="container">
                    <div class="centered">
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle">About Us</h1><hr class="titleHR" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil beatae autem blanditiis fugiat perspiciatis, ea rem, provident vel facere quibusdam ipsa eum impedit placeat, exercitationem facilis! Aliquid blanditiis iste magni!</p>
                        
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle">What We Do</h1><hr class="titleHR" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ad ipsam ut beatae fuga delectus asperiores. Aut exercitationem quaerat id libero a vel dolores, magnam. Repellendus magnam nisi quidem ex!</p>
                        
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle">Websites</h1><hr class="titleHR" />
                        <img class="exampleImage" src="images/assets/clear/iMac.png" alt="iMac">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates voluptatibus officiis, quidem omnis repudiandae esse possimus ratione saepe, cupiditate quibusdam blanditiis fuga explicabo culpa minima ab beatae nihil. Consectetur?</p>
                        
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle">Software</h1><hr class="titleHR" />
                        <img class="exampleImage" src="images/assets/clear/Macbook%20Pro%20and%20Screen.png" alt="iMac">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates voluptatibus officiis, quidem omnis repudiandae esse possimus ratione saepe, cupiditate quibusdam blanditiis fuga explicabo culpa minima ab beatae nihil. Consectetur?</p>
                        
                        <div class="topMarginLarge"></div>
                        <hr class="titleHR" /><h1 class="inlineMiddle">Apps</h1><hr class="titleHR" />
                        <img class="exampleImage" src="images/assets/clear/iPad%20Macbook%20iPhone.png" alt="iMac">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque voluptates voluptatibus officiis, quidem omnis repudiandae esse possimus ratione saepe, cupiditate quibusdam blanditiis fuga explicabo culpa minima ab beatae nihil. Consectetur?</p>
                    </div>
                </div>
            </div>
        </div>
        
        
        <?php include_once 'templates/footer.php';?>
    </body>
    
</html>