<html>
    <head>
        <meta name=viewport content="width=device-width, initial-scale=1">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="styles/all.css">
        <link rel="stylesheet" href="styles/desktop.css" media="(min-width: 1024px)">
        <link rel="stylesheet" href="styles/laptop.css" media="(min-width: 680px) and (max-width: 1024px)">
        <link rel="stylesheet" href="styles/mobile.css" media="(max-width: 680px)">
        <link rel="stylesheet" href="styles/slider.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>