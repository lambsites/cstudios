<?php
// The message
$message = "Message From:" . $_POST['ContactName'] . ", " . $_POST['ContactEmail'] . ", " . $_POST['ContactPhone'] . "\r\n\r\nMessage:\r\n" . $_POST['ContactMessage'];

// In case any of our lines are larger than 70 characters, we should use wordwrap()
$message = wordwrap($message, 70, "\r\n");

// Send
mail('contact@cstudios.co.uk', 'CONTACT - ' . $_POST['ContactEmail'], $message);

header("Location: http://cstudios.co.uk/index.php");
?>