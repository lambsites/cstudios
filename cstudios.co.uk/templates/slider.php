<script type="application/javascript" src="js/slider.js"></script>
<div class="wrapper">
    <div class="slider">
        <div id="imageSlideArea" class="imageSlideArea">
            <img id="firstSlideImage" src="images/banner/websitedesigndevelopment.png" alt="">
            <img src="images/banner/softwareappdevelopment.png" alt="">
        </div>
        <ul class="imageSlideMenu">
            <div class="container">
                <li><a onclick="slidePause(0)">Website Design and Development</a></li>
                <li><a onclick="slidePause(1)">Software and App Development</a></li>
            </div>
        </ul>
    </div>
</div>
<script type="application/javascript">

window.onload = function () {setSlideHeight()}
</script>