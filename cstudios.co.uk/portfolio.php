<?php include_once 'php/htmlHead.php';?>
    </head>
    
    
    
    <body>
        <?php include_once 'templates/header.php';?>
        <div class="secondaryNavWrapper">
            <ul class="secondaryNav">
                <li <?php if($_GET['type']=="website"){echo 'class="active"';} ?>><a href="portfolio.php?type=website">Websites</a></li>
                <li <?php if($_GET['type']=="mobile"){echo 'class="active"';} ?>><a href="portfolio.php?type=mobile">Mobile Apps</a></li>
                <li <?php if($_GET['type']=="desktop"){echo 'class="active"';} ?>><a href="portfolio.php?type=desktop">Desktop Apps</a></li>
                <li <?php if($_GET['type']=="templates"){echo 'class="active"';} ?>><a href="portfolio.php?type=templates">Website Templates</a></li>
            </ul>
        </div>
        
        <div class="wrapper">
            <div class="pageContent">
                <div class="container">
                    <div class="centered">
                        <?php
                        
                        if (isset($_GET['type'])) {
                            switch ($_GET['type']) {
                                case 'website':
                                    ?>
                                    
                                                                                        <!-- WEBSITE PORTFOLIO -->
                                        <div class="topMarginLarge"></div>
                                        <a class="portfolioLink" href="http://walkwoofwag.cstudios.co.uk/">
                                            <hr class="titleHR" /><h1 class="inlineMiddle">Walk Woof Wag</h1><hr class="titleHR" />
                                            <img src="images/sites/WalkWoofWag/iPad%20Macbook%20iPhone%20WalkWoofWag.png" alt="" class="exampleImage">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil beatae autem blanditiis fugiat perspiciatis, ea rem, provident vel facere quibusdam ipsa eum impedit placeat, exercitationem facilis! Aliquid blanditiis iste magni!</p>
                                        </a>
                                    
                                    <?php
                                    break;
                                case 'mobile':
                                    ?>
                                    
                                                                                        <!-- MOBILE PORTFOLIO -->
                                    
                                    <?php
                                    break;
                                case 'desktop':
                                    ?>
                                    
                                                                                        <!-- DESKTOP PORTFOLIO -->
                                    
                                    <?php
                                    break;
                                case 'templates':
                                    ?>
                                    
                                                                                        <!-- TEMPLATE PORTFOLIO -->
                                    
                                    <?php
                                    break;
                            }
                        }
                        
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
        
        
        <?php include_once 'templates/footer.php';?>
    </body>
    
</html>