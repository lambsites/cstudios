#Hydrant Install Profile

The Hydrant Install Profile enables a selection of contributed modules, which are used from day to day on pretty muchy every site Hydrant will build.

It also configures a number of options, which would typically be made. These are detailed below:


## A "Basic page" content type

With the following fields:

* Basic content vertical tab containing all fields
* Title
* Body
* Attachments

and the following settings:

* Preview, promote frontpage and date & author information disabled.
* Revisions enabled
* Menu choices for "main menu" and "footer menu"
* XML sitemap inclusion


## A "Webform" content type

Extending the standard enabling of the "webform" module, with the following fields:

* Basic content vertical tab containing all fields
* Title
* Body
* Attachments.

and the following settings:

* Preview, promote frontpage and date & author information disabled.
* Revisions enabled
* Menu choices for "main menu" and "footer menu"
* XML sitemap inclusion


## A “filtered html” text format

Recommended text format options, to restrict what html tags and attributes can be used.


## Backup Migrate schedule

A schedule, to backup the web site database one a day, keeping copies for a week. This setup is created from a "feature" module, which is enabled by the install profile.


## Menus

* A footer menu, with it's block assigned to the "footer" region of the site theme
* The main menu block assigned to the "navigation" region of the theme


## Existing content administration

A "view", which is stored as a "feature" and enabled by the install profile.

* Title, Type, Created, Updated, Author, Published, Edit , Delete columns
* Bulk operations to delete, publish, unpublish & update url aliases
* Choose how many results show per page


## General settings

* Disable user registration
* Set error level to warnings only.
* Use hydrant admin theme
* Use admin theme for editing and creating content
* Use node title and menu parent if set as format for url aliases
* Set short, medium and long date to UK formats
* Set the private folder path
* Set the first day of the week to Monday instead of Sunday
* Turn the option for users to set their own timezone off
* Expand the views ui “advanced” options to be open by default
* Set “less” devel mode to be on by default
* Set less watch mode to be off by default
* Ability to add Google Analytics tracking code
* Use Hydrant Bootstrap theme as default front end theme instead of Bartik
* Disable the Bartik theme
* Tell menu position to mark items as active, rather than adding a link into the menu


## Administrator role

* Access the admin theme and toolbar
* View and revert revisions
* Create / edit / delete “page” content
* Publish / Unpublish “page” content
* Create / edit / delete users
* Assign the administrator role to other users
* Access all webform results
* Edit meta tags
* Administer 301 redirects
* Access Google Analytics reports
* Administer main menu items
* Administer footer menu items
* Create / edit / delete “webform” content
* Publish / unpublish “webform” content


## CKeditor profile

* Format (heading 2, heading 3 etc)
* Bold
* Strikethrough
* Subscript / superscript
* Italic
* Numbered / bulleted lists
* Link / Unlink
* Embed media (such as Youtube and Vimeo videos)
* Insert tables
* Paste as plain text
* Enforce plain text pasting


## Password policy

Enforce stronger passwords


## Re route email

Specify an email address for development sites, so that emails are never sent out to the public by mistake.


## jQuery update

Use jQuery update for the front end theme.
* The backend theme should be set to use the default version supplied by Drupal (1.4)
* Bootstrap 3 based themes require jQuery 1.8 or above.


## Transliteration

Automatically correct any filenames that include special characters


## Site verification

Use the site_verify module to add meta tags for the likes of Google Webmaster Tools or Bing.

