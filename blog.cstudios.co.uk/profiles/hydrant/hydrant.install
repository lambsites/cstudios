<?php

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 */
function hydrant_install() {
  hydrant_install_create_field_bases();
  hydrant_install_create_node_types();
  hydrant_install_create_menu();
  hydrant_install_setup_shortcuts();
  hydrant_install_standard_settings();
  hydrant_install_create_admin_role();
  hydrant_install_add_text_formats();
  hydrant_install_default_permissions();
  hydrant_install_configure_imce();
  hydrant_install_configure_ckeditor();
  hydrant_install_dashboard_blocks();
  hydrant_install_password_policy();
  hydrant_install_reroute_email();
  hydrant_install_jquery_update();
  hydrant_install_menu_position_update();
  hydrant_install_block_assignment();
  hydrant_install_themes();
  hydrant_install_toolbar();
  hydrant_install_hydrant_start();
}


/**
 * Create any field bases
 */
function hydrant_install_create_field_bases() {

  //Attachments base
  $field = array(
    'field_name' => 'field_attachments',
    'type' => 'file',
    'cardinality' => -1,
    'locked' => FALSE,
    'indexes' => array('fid' => array('fid')),
    'settings' => array(
      'uri_scheme' => 'public',
    ),
    'storage' => array(
      'type' => 'field_sql_storage',
      'settings' => array(),
    ),
  );
  field_create_field($field);

}


/**
 * Create standard "Basic page" node type.
 */
function hydrant_install_create_node_types() {
  $types = array(
    array(
      'type' => 'page',
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t("Create a basic page of content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );
  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
  }

  // Create and attach the body field on page.
  $field = field_info_field('body');
  $instance = field_info_instance('node', 'body', 'page');
  if (empty($field)) {
    $field = array(
      'field_name' => 'body',
      'type' => 'text_with_summary',
      'entity_types' => array('node'),
    );
    field_create_field($field);
  }
  if (empty($instance)) {
    $instance = array(
      'field_name' => 'body',
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => 'Body',
      'widget' => array(
        'type' => 'text_textarea_with_summary',
        'weight' => 10,
      ),
      // This is overridden from TRUE to FALSE.
      'settings' => array('display_summary' => FALSE),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'text_default',
          'weight' => 10,
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_summary_or_trimmed',
          'weight' => 10,
        ),
      ),
    );
    field_create_instance($instance);
  }

  // Create and attach the attachments field
  $instance = array(
    'field_name' => 'field_attachments',
    'entity_type' => 'node',
    'label' => 'Attachments',
    'bundle' => 'page',
    'required' => FALSE,

    'settings' => array(
      'file_directory' => 'documents/pages',
      'file_extensions' => 'pdf doc docx xls xlsx',
      'description_field' => 1,
    ),

    'widget' => array(
      'type' => 'file_generic',
      'weight' => 11,
    ),

    'display' => array(
      'default' => array(
        'type' => 'file_default',
        'weight' => 11,
      ),
      'teaser' => array(
        'type' => 'file_default',
        'weight' => 11,
      ),
    ),
  );
  field_create_instance($instance);

  // Create and attach a field group to the basic page
  $group = (object) array(
	  'identifier' => 'group_page_basics|node|page|form',
	  'group_name' => 'group_page_basics',
	  'entity_type' => 'node',
	  'bundle' => 'page',
	  'mode' => 'form',
	  'label' => 'Basic settings',
	  'children' => array(
		  0 => 'body',
      1 => 'title',
      2 => 'field_attachments',
	  ),
	  'weight' => '0',
	  'format_type' => 'tab',
	  'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basics field-group-tab',
        'required_fields' => 1,
      ),
    ),
	);
	field_group_group_save($group);

	// Create and attach a field group to the webform content type
  $group = (object) array(
	  'identifier' => 'group_webform_basics|node|webform|form',
	  'group_name' => 'group_webform_basics',
	  'entity_type' => 'node',
	  'bundle' => 'webform',
	  'mode' => 'form',
	  'label' => 'Basic settings',
	  'children' => array(
      0 => 'title',
      1 => 'body',
      2 => 'field_attachments',
	  ),
	  'weight' => '0',
	  'format_type' => 'tab',
	  'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basics field-group-tab',
        'required_fields' => 1,
      ),
    ),
	);
	field_group_group_save($group);

  // Create and attach the body field to webform.
  $field = field_info_field('body');
  $instance = field_info_instance('node', 'body', 'webform');
  if (empty($field)) {
    $field = array(
      'field_name' => 'body',
      'type' => 'text_with_summary',
      'entity_types' => array('node'),
    );
    field_create_field($field);
  }
  if (empty($instance)) {
    $instance = array(
      'field_name' => 'body',
      'entity_type' => 'node',
      'bundle' => 'webform',
      'label' => 'Body',
      'widget' => array(
        'type' => 'text_textarea_with_summary',
        'weight' => 9,
      ),
      // This is overridden from TRUE to FALSE.
      'settings' => array('display_summary' => FALSE),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'text_default',
          'weight' => 9,
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_summary_or_trimmed',
          'weight' => 9,
        ),
      ),
    );
    field_create_instance($instance);
  }

  $instance = array(
    'field_name' => 'field_attachments',
    'entity_type' => 'node',
    'label' => 'Attachments',
    'bundle' => 'webform',
    'required' => FALSE,

    'settings' => array(
      'file_directory' => 'documents/webforms',
      'file_extensions' => 'pdf doc docx xls xlsx',
      'description_field' => 1,
    ),

    'widget' => array(
      'type' => 'file_generic',
      'weight' => 11,
    ),

    'display' => array(
      'default' => array(
        'type' => 'file_default',
        'weight' => 11,
      ),
      'teaser' => array(
        'type' => 'file_default',
        'weight' => 11,
      ),
    ),
  );
  field_create_instance($instance);


  // Set the pathauto for page content type.
  variable_set('pathauto_node_pattern', '[node:menu-link:parent:title]/[node:title]');

  // "Basic page" form settings.
  // Do not promote to frontpage.
  variable_set('node_options_page', array('status', 'revision'));
  // Date and author information disabled.
  variable_set('node_submitted_page', FALSE);
  // Preview button disabled.
  variable_set('node_preview_page', '0');
  //Set available menus
  variable_set('menu_options_page', array('footer-menu', 'main-menu'));
  //Set default menu
  variable_set('menu_parent_page', 'main-menu:0');
  //XML sitemap
  variable_set('xmlsitemap_settings_node_page', array('status' => 1, 'priority' => 0.5));


  // "Webform" form settings.
  // Do not promote to frontpage.
  variable_set('node_options_webform', array('status', 'revision'));
  // Date and author information disabled.
  variable_set('node_submitted_webform', FALSE);
  // Preview button disabled.
  variable_set('node_preview_webform', '0');
  //Set available menus
  variable_set('menu_options_webform', array('footer-menu', 'main-menu'));
  //Set default menu
  variable_set('menu_parent_webform', 'main-menu:0');
  //XML sitemap
  variable_set('xmlsitemap_settings_node_webform', array('status' => 1, 'priority' => 0.5));

}


/**
 * Create any menus
 */
function hydrant_install_create_menu() {

  $menu = array(
    'menu_name' => 'footer-menu', // Machine name
    'title' => 'Footer menu', // Display name
    'description' => 'The menu which appears in the footer of thee website.',
  );

  menu_save($menu);

}


/**
 * Create default shortcut toolbar entries.
 */
function hydrant_install_setup_shortcuts() {

  $shortcut_set = shortcut_set_load(SHORTCUT_DEFAULT_SET_NAME);
  $shortcut_set->links = array(
    array(
      'link_path' => 'admin/structure/menu',
      'link_title' => t('Menus'),
      'weight' => -19,
    ),
    array(
      'link_path' => 'admin/structure/taxonomy',
      'link_title' => t('Taxonomy'),
      'weight' => -19,
    ),
    array(
      'link_path' => 'admin/config/search/metatags',
      'link_title' => t('Metatags'),
      'weight' => -19,
    ),
  );

  shortcut_set_save($shortcut_set);

  db_delete('menu_links')->condition('link_title', 'Find content')->execute();

}


/**
 * Configure various default settings.
 */
function hydrant_install_standard_settings() {
  // Users can only be created by the administrator.
  variable_set('user_register', USER_REGISTER_ADMINISTRATORS_ONLY);

  // Set the error reporting to "Errors and warnings".
  variable_set('error_level', 1);

  // Do not add redirects when url aliases are changed
  variable_set('redirect_auto_redirect', 0);

  // Set the admin theme.
  variable_set('admin_theme', 'hydrantadmin');
  variable_set('node_admin_theme', '1');
  variable_set('admin_theme_admin_theme_batch', '1');
  variable_set('admin_theme_admin_theme_webform_results', '1');

  // Maxlength is enforced for user 1 (Does not work with Maxlength module v 3.0. Need dev or higher.)
  variable_set('maxlength_always_for_uid1', TRUE);

  // Regional settings
  variable_set('date_first_day', '1');
  variable_set('user_default_timezone', FALSE);

  // Set default theme
  theme_enable(array('hydrant_bootstrap'));

  $hydrant_bootstrap_settings = array(
    'toggle_logo' => 1,
    'toggle_name' => 0,
    'toggle_slogan' => 0,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 0,
    'toggle_secondary_menu' => 0,
    'default_logo' => 1,
    'logo_path' => NULL,
    'logo_upload' => NULL,
    'default_favicon' => 1,
    'favicon_path' => NULL,
    'favicon_upload' => NULL,
    'general__active_tab' => 'edit-favicon',
    'bootstrap__active_tab' => 'edit-components',
    'bootstrap_breadcrumb' => 1,
    'bootstrap_breadcrumb_home' => 1,
    'bootstrap_breadcrumb_title' => 1,
    'bootstrap_navbar_position' => NULL,
    'bootstrap_navbar_inverse' => 0,
    'bootstrap_region_well-navigation' => NULL,
    'bootstrap_region_well-header' => NULL,
    'bootstrap_region_well-highlighted' => NULL,
    'bootstrap_region_well-help' => NULL,
    'bootstrap_region_well-content' => NULL,
    'bootstrap_region_well-sidebar_first' => 'well',
    'bootstrap_region_well-sidebar_second' => NULL,
    'bootstrap_region_well-footer' => NULL,
    'bootstrap_region_well-page_top' => NULL,
    'bootstrap_region_well-page_bottom' => NULL,
    'bootstrap_region_well-dashboard_main' => NULL,
    'bootstrap_region_well-dashboard_sidebar' => NULL,
    'bootstrap_region_well-dashboard_inactive' => NULL,
    'bootstrap_anchors_fix' => 1,
    'bootstrap_anchors_smooth_scrolling' => 1,
    'bootstrap_popover_enabled' => 1,
    'bootstrap_popover_animation' => 1,
    'bootstrap_popover_html' => 0,
    'bootstrap_popover_placement' => 'right',
    'bootstrap_popover_selector' => NULL,
    'bootstrap_popover_trigger' => array(
      'click' => 'click',
      'hover' => 0,
      'focus' => 0,
      'manual' => 0,
    ),
    'bootstrap_popover_title' => NULL,
    'bootstrap_popover_content' => NULL,
    'bootstrap_popover_delay' => 0,
    'bootstrap_popover_container' => 'body',
    'bootstrap_tooltip_enabled' => 1,
    'bootstrap_tooltip_descriptions' => 1,
    'bootstrap_tooltip_animation' => 1,
    'bootstrap_tooltip_html' => 0,
    'bootstrap_tooltip_placement' => 'auto left',
    'bootstrap_tooltip_selector' => NULL,
    'bootstrap_tooltip_trigger' => array(
      'hover' => 'hover',
      'focus' => 'focus',
      'click' => 0,
      'manual' => 0,
    ),
    'bootstrap_tooltip_delay' => 0,
    'bootstrap_tooltip_container' => 'body',
    'bootstrap_cdn' => '3.0.2',
    'bootstrap_bootswatch' => NULL,
    'bootstrap_rebuild_registry' => 0,
    'bootstrap_toggle_jquery_error' => 0,
  );

  variable_set('theme_default', 'hydrant_bootstrap');
  variable_set('theme_hydrant_bootstrap_settings', $hydrant_bootstrap_settings);

  // Set the date formats (formats must exist in the date_formats table).
  variable_set('date_format_short', 'd/m/Y - g:ia');
  variable_set('date_format_medium', 'j F Y - g:ia');
  variable_set('date_format_long', 'l, j F Y - g:ia');

  // Expand the advanced views column by default.
  variable_set('views_ui_show_advanced_column', TRUE);

  // Set the private file path.
  variable_set('file_private_path', 'sites/default/files/private');

  // Less developer mode.
  variable_set('less_devel', 1);
  variable_set('less_watch', 0);

  // Metatags settings
  variable_set('metatag_enable_taxonomy_term', 0);
  variable_set('metatag_enable_user', 0);

}


/**
 * Create the administrator role by default.
 */
function hydrant_install_create_admin_role() {
  // Create administrator role.
  $admin_role = new stdClass();
  $admin_role->name = 'Administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
}


/**
 * Create standard text formats.
 */
function hydrant_install_add_text_formats() {
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => 0,
    'filters' => array(
      // Image resize filter
      'image_resize_filter' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'link' => 0,
          'link_class' => '',
          'link_rel' => '',
          'image_locations' => array(
            'local' => 'local',
            'remote' => 'remote',
          ),
        ),
      ),
      // URL filter.
      'filter_url' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // HTML filter.
      'filter_html' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(
          'allowed_html' => "<a> <em> <strong> <cite> <blockquote> <ul> <ol> <li> <h2> <h3> <h4> <h5> <p> <br> <sup> <sub> <s> <iframe> <img> <table> <thead> <tbody> <tr> <th> <td>"
        ),
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 3,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);
  $filtered_html_permission = filter_permission_name($filtered_html_format);
  user_role_grant_permissions(3, array($filtered_html_permission));
}


/**
 * Set up some default permissions.
 */
function hydrant_install_default_permissions() {
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array(
    'access content'
  ));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array(
    'access content'
  ));
  user_role_grant_permissions(3, array(
    'access admin theme',
    'access administration pages',
    'administer main-menu menu items',
    'administer footer-menu menu items',
    'access ckeditor link',
    'view revisions',
    'revert revisions',
    'create page content',
    'edit any page content',
    'delete any page content',
    'create webform content',
    'edit any webform content',
    'delete any webform content',
    'override page published option',
    'override webform published option',
    'assign Administrator role',
    'view the administration theme',
    'access toolbar',
    'create users',
    'access users overview',
    'cancel users with role 2',
    'cancel users with role 3',
    'edit users with role 2',
    'edit users with role 3',
    'access all webform results',
    'administer meta tags',
    'edit meta tags',
    'administer redirects',
    'access dashboard',
    'edit webform components',
  ));
}

function hydrant_install_configure_imce() {
  // IMCE setting
	$imce_options = array(
    1 =>
    array (
      'name' => 'User-1',
      'usertab' => 0,
      'filesize' => '0',
      'quota' => '0',
      'tuquota' => '0',
      'extensions' => '*',
      'dimensions' => '1200x1200',
      'filenum' => '0',
      'directories' =>
      array (
        0 =>
        array (
          'name' => 'images/inline-files',
          'subnav' => 1,
          'browse' => 1,
          'upload' => 1,
          'thumb' => 1,
          'delete' => 1,
          'resize' => 1,
        ),
      ),
      'thumbnails' =>
      array (
        0 =>
        array (
          'name' => 'Small',
          'dimensions' => '90x90',
          'prefix' => 'small_',
          'suffix' => '',
        ),
        1 =>
        array (
          'name' => 'Medium',
          'dimensions' => '120x120',
          'prefix' => 'medium_',
          'suffix' => '',
        ),
        2 =>
        array (
          'name' => 'Large',
          'dimensions' => '180x180',
          'prefix' => 'large_',
          'suffix' => '',
        ),
      ),
    ),
    2 =>
    array (
      'name' => 'Sample profile',
      'usertab' => 1,
      'filesize' => 1,
      'quota' => 2,
      'tuquota' => 0,
      'extensions' => 'gif png jpg jpeg',
      'dimensions' => '800x600',
      'filenum' => 1,
      'directories' =>
      array (
        0 =>
        array (
          'name' => 'u%uid',
          'subnav' => 0,
          'browse' => 1,
          'upload' => 1,
          'thumb' => 1,
          'delete' => 0,
          'resize' => 0,
        ),
      ),
      'thumbnails' =>
      array (
        0 =>
        array (
          'name' => 'Thumb',
          'dimensions' => '90x90',
          'prefix' => 'thumb_',
          'suffix' => '',
        ),
      ),
    ),
  );

  // Assign IMCE to roles
  $imce_roles_profiles = array (
    3 =>
    array (
      'public_pid' => '1',
      'private_pid' => '1',
    ),
    2 =>
    array (
      'public_pid' => 0,
      'private_pid' => 0,
    ),
    1 =>
    array (
      'public_pid' => 0,
      'private_pid' => 0,
    ),
  );

  // Save IMCE settings
	variable_set('imce_profiles', $imce_options);
	variable_set('imce_roles_profiles', $imce_roles_profiles);
	variable_set('imce_settings_absurls', 0);
	variable_set('imce_settings_disable_private', 1);
	variable_set('imce_settings_replace', '0');
	variable_set('imce_settings_textarea', '');
	variable_set('imce_settings_thumb_method', 'scale_and_crop');
}


/**
 * Set up CKeditor settings.
 * To update this - create a feature to source the code to add here
 */
function hydrant_install_configure_ckeditor() {
  $result = db_select('ckeditor_settings', 's')
    ->fields('s')
    ->condition('name', 'Advanced', '=')
    ->execute()
    ->fetchAssoc();
  $result['settings'] = unserialize($result['settings']);
  // Force paste as plain text.
  $result['settings']['forcePasteAsPlainText'] = 't';
  // Use ckeditor default style.
  $result['settings']['css_style'] = 'default';
  $result['settings']['css_mode'] = 'self';
  $result['settings']['css_path'] = '%tcss/ckeditor.css';
  $result['settings']['js_conf'] = "
config.allowedContent = true;
config.extraPlugins = 'lineutils';
config.extraPlugins = 'widget';
config.extraPlugins = 'image2';
config.image2_alignClasses = [ 'left', 'center', 'right' ];
  ";
  $result['settings']['loadPlugins'] = array(
    'ckeditor_link' => array(
      'name' => 'drupal_path',
      'desc' => 'CKEditor Link - A plugin to easily create links to Drupal internal paths',
      'path' => '%base_path%sites/all/modules/contrib/ckeditor_link/plugins/link/',
      'buttons' => NULL,
    ),
    'drupalbreaks' => array(
      'name' => 'drupalbreaks',
      'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
      'path' => '%plugin_dir%drupalbreaks/',
      'buttons' => array(
        'DrupalBreak' => array(
          'label' => 'DrupalBreak',
          'icon' => 'images/drupalbreak.png',
        ),
      ),
      'default' => 't',
    ),
    'mediaembed' => array(
      'name' => 'mediaembed',
      'desc' => 'Plugin for inserting Drupal embeded media',
      'path' => '%plugin_dir%mediaembed/',
      'buttons' => array(
        'MediaEmbed' => array(
          'label' => 'MediaEmbed',
          'icon' => 'images/icon.png',
        ),
      ),
      'default' => 'f',
    ),
  );
  //define format options
  $result['settings']['font_format'] = "p;h1;h2;h3;h4";
  // Define the editor buttons.
  $result['settings']['toolbar'] = "[['Format','Bold','Italic','-','Blockquote','Strike','Subscript','Superscript','-','NumberedList','BulletedList','-','Link','Unlink','PasteText','MediaEmbed','Table','-','Image']]";
  // Set IMCE to be file browser
  $result['settings']['filebrowser'] = "imce";
  db_update('ckeditor_settings')->fields(
    array(
      'settings' => serialize($result['settings']),
    )
  )
  ->condition('name', 'Advanced', '=')
  ->execute();
}


/**
 * Arranges blocks on the dashboard.
 */
function hydrant_install_dashboard_blocks() {
  db_insert('block')->fields(
    array(
      'bid' => '27',
      'module' => 'views',
      'delta' => 'webform_submissions-block',
      'theme' => 'hydrantadmin',
      'status' => '1',
      'weight' => '0',
      'region' => 'dashboard_sidebar',
      'custom' => '0',
      'visibility' => '0',
      'pages' => '',
      'title' => '',
      'cache' => '-1'
    )
  )->execute();

  db_insert('block')->fields(
    array(
      'bid' => '28',
      'module' => 'views',
      'delta' => 'existing_content-block_1',
      'theme' => 'hydrantadmin',
      'status' => '1',
      'weight' => '0',
      'region' => 'dashboard_main',
      'custom' => '0',
      'visibility' => '0',
      'pages' => '',
      'title' => '',
      'cache' => '-1'
    )
  )->execute();
}


/**
 * Set password policy
 */
function hydrant_install_password_policy() {

	// Password policy options
	$password_policy_options = array(
    'digit' => '1',
    'length' => '8',
    'lowercase' => '1',
    'uppercase' => '1',
    'username' => '1',
  );

	// Create the new password policy
  db_insert('password_policy')->fields(
    array(
      'pid' => '1',
      'name' => 'Default password policy',
      'description' => 'Sets the default password policy for the site',
      'enabled' => '1',
      'policy' => serialize($password_policy_options)
    )
  )->execute();

  // Assign the new policy to user roles
  // Assign to Administrator
  db_insert('password_policy_role')->fields(
    array(
      'pid' => '1',
      'rid' => '3'
    )
  )->execute();

  // Assign to Authencated User
  db_insert('password_policy_role')->fields(
    array(
      'pid' => '1',
      'rid' => '2'
    )
  )->execute();

}


/**
 * Make sure emails are being re-routed to watchdog.
 */
function hydrant_install_reroute_email() {
  // Re-route email by default.
  variable_set(REROUTE_EMAIL_ENABLE, 1);
  // Don't send it anywhere, just watchdog it.
  variable_set(REROUTE_EMAIL_ADDRESS, '');
}


/**
 * Configure jQuery update to use the default provided by Drupal for admin pages.
 */
function hydrant_install_jquery_update() {

  variable_set('jquery_update_jquery_admin_version', 'default');

  variable_set('jquery_update_jquery_version', '1.10');

}


/**
 * Set the menu position settings
 */
function hydrant_install_menu_position_update() {

  variable_set('menu_position_active_link_display', 'parent');

}


/**
 * Assign generic blocks to regions
 */
function hydrant_install_block_assignment() {

  $default_theme = variable_get('theme_default');

  $blocks = array(
    array(
      'module' => 'system',
      'delta' => 'main-menu',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'navigation',
      'visibility' => 0,
      'pages' => '',
      'title' => '<none>',
      'cache' => -1,
    ),
    array(
      'module' => 'menu',
      'delta' => 'footer-menu',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'footer',
      'visibility' => 0,
      'pages' => '',
      'title' => '<none>',
      'cache' => -1,
    ),
  );

  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'visibility', 'pages', 'title', 'cache'));

  foreach ($blocks as $block) {
    $query->values($block);
  }

  $query->execute();

}

/**
 * Set the available themes
 */
function hydrant_install_themes() {
  // Disable Bartik
	theme_disable(array('bartik'));
}

/**
 * Disable the Drupal toolbar module as its not needed
 */
function hydrant_install_toolbar() {
  module_disable(array('toolbar'));
}

/**
 * If Hydrant Start module is available install it the module
 */
function hydrant_install_hydrant_start() {
  module_enable(array('hydrant_start'));
}
