<?php
/**
 * Override or insert variables into the page template.
 */
function hydrantadmin_preprocess_page(&$vars) {
  $vars['primary_local_tasks'] = $vars['tabs'];
  unset($vars['primary_local_tasks']['#secondary']);
  $vars['secondary_local_tasks'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );

  //Shortcut icons
  $shortcuticon = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/l/apple-touch-icon.png'
    ),
  );

  $appleIconLow = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/l/apple-touch-icon-precomposed.png',
      'rel' => 'apple-touch-icon-precomposed'
    ),
  );

  $appleIconMedium = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/m/apple-touch-icon.png',
      'rel' => 'apple-touch-icon-precomposed',
      'sizes' => '72x72'
    ),
  );

  $appleIconHigh = array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => base_path().path_to_theme().'/img/siteicons/h/apple-touch-icon.png',
      'rel' => 'apple-touch-icon-precomposed',
      'sizes' => '114x114'
    ),
  );

  drupal_add_html_head($shortcuticon, 'shortcuticon');
  drupal_add_html_head($appleIconLow, 'touch-icon-low');
  drupal_add_html_head($appleIconMedium, 'touch-icon-medium');
  drupal_add_html_head($appleIconHigh, 'touch-icon-high');

}

function hydrantadmin_breadcrumb($variables) {

  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {

    //Override the first breadcrumb item to link to the dashboard instead of the homepage
    $breadcrumb[0] = l(t('Dashboard'), 'admin/dashboard');

    //Append the current page title to the end of the breadcrumb
    $breadcrumb[] = drupal_get_title();

    //Neaten up the dashboards breadcrumb
    if (arg(0) == 'admin' && arg(1) == 'dashboard') {
      $breadcrumb = array();
      $breadcrumb[] = drupal_get_title();
      drupal_set_breadcrumb($breadcrumb);
    } else if (arg(0) == 'admin' && arg(1) == 'existing-content') {
      $breadcrumb = array();
      $breadcrumb[] = l(t('Dashboard'), 'admin/dashboard');
      $breadcrumb[] = drupal_get_title();
      drupal_set_breadcrumb($breadcrumb);
    }

    $output = '<div class="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>';

    return $output;

  }
}

/**
 * Override of theme_tablesort_indicator().
 *
 * Use our own image versions, so they show up as black and not gray on gray.
 */
function hydrantadmin_tablesort_indicator($variables) {
  $style = $variables['style'];
  $theme_path = drupal_get_path('theme', 'hydrantadmin');
  if ($style == 'asc') {
    return theme('image', array('path' => $theme_path . '/img/arrow-asc.png', 'alt' => t('sort ascending'), 'width' => 13, 'height' => 13, 'title' => t('sort ascending')));
  }
  else {
    return theme('image', array('path' => $theme_path . '/img/arrow-desc.png', 'alt' => t('sort descending'), 'width' => 13, 'height' => 13, 'title' => t('sort descending')));
  }
}
