<!doctype html>
<!--[if lte IE 6]><html class="no-js ie6 ie" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js ie7 ie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 ie" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->

<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<body id="scrollto">

<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>

</body>
</html>