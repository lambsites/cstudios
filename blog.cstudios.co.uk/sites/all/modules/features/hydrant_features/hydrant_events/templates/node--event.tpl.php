<?php if ($teaser) { ?>


  <div itemscope itemtype="http://schema.org/Event">

    <?php if (!empty($content['field_event_img'])) { ?>
      <div class="col-xs-12 col-sm-3">
        <?php print render($content['field_event_img']); ?>
      </div>
    <?php } ?>

    <div class="listing-summary col-xs-12<?php if (!empty($content['field_event_img'])) { print ' col-sm-9'; } ; ?>">

      <h3 class="title" itemprop="name">
        <a href="<?php print $node_url; ?>" itemprop="url">
          <?php print $title; ?>
        </a>
      </h3>

      <?php if (!empty($date_prefix)) { ?>
        <div class="event-passed">
          <?php print $date_prefix; ?>
        </div>
      <?php } ?>

      <div class="date">
        <?php print render($content['field_event_date']); ?>
      </div>

      <div itemprop="description">
        <?php print render($content['body']); ?>
      </div>

      <?php
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      ?>

    </div>

  </div>


<?php } else { ?>


  <div itemscope itemtype="http://schema.org/Event">

    <div class="listing-content">

      <?php if (!empty($content['field_event_img'])) { ?>
        <?php print render($content['field_event_img']); ?>
      <?php } ?>

      <?php if (!empty($date_prefix)) { ?>
        <p class="event-passed">
          <?php print $date_prefix; ?>
        </p>
      <?php } ?>

      <div class="date">
        <?php print render($content['field_event_date']); ?>
      </div>

      <div itemprop="articleBody">
        <?php print render($content['body']); ?>
      </div>

      <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      ?>

    </div>

  </div>


<?php } ?>



