<?php
/**
 * @file
 * hydrant_search_autocomplete.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hydrant_search_autocomplete_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_search_autocomplete_config_features_default_settings().
 */
function hydrant_search_autocomplete_search_autocomplete_config_features_default_settings() {
  return array(
    0 => array(
      'fid' => 1,
      'title' => 'Search page - Node Tab  (search/node/%)',
      'selector' => 'input#edit-keys.form-control.form-text',
      'weight' => 0,
      'enabled' => 1,
      'parent_fid' => 0,
      'min_char' => 3,
      'max_sug' => 10,
      'no_results' => '{"label":"No results found for [search-phrase]. Click to perform full search.","value":"[search-phrase]","link":"","group":{"group_id":"no_results"}}',
      'all_results' => '{"label":"View all results for [search-phrase].","value":"[search-phrase]","link":"","group":{"group_id":"all_results"}}',
      'auto_submit' => 1,
      'auto_redirect' => 1,
      'translite' => 1,
      'data_source' => 'view',
      'data_callback' => 'search_autocomplete/autocomplete/1/',
      'data_static' => '',
      'data_view' => 'nodes_autocomplete',
      'theme' => 'basic-green.css',
    ),
    1 => array(
      'fid' => 2,
      'title' => 'Search page - User Tab  (search/user/%)',
      'selector' => '#search-form[action="/search/user"] #edit-keys',
      'weight' => 1,
      'enabled' => 0,
      'parent_fid' => 0,
      'min_char' => 3,
      'max_sug' => 10,
      'no_results' => '{"label":"No results found for [search-phrase]. Click to perform full search.","value":"[search-phrase]","link":"","group":{"group_id":"no_results"}}',
      'all_results' => '{"label":"View all results for [search-phrase].","value":"[search-phrase]","link":"","group":{"group_id":"all_results"}}',
      'auto_submit' => 1,
      'auto_redirect' => 1,
      'translite' => 1,
      'data_source' => 'view',
      'data_callback' => 'search_autocomplete/autocomplete/2/',
      'data_static' => '',
      'data_view' => 'users_autocomplete',
      'theme' => 'user-blue.css',
    ),
  );
}
