<?php if ($teaser) { ?>

  <?php if (!empty($content['field_blog_img'])) { ?>
    <div class="col-xs-12 col-sm-3">
      <?php print render($content['field_blog_img']); ?>
    </div>
  <?php } ?>

  <div class="listing-summary col-xs-12<?php if (!empty($content['field_blog_img'])) { print ' col-sm-9'; } ; ?>">

    <h3 class="title header-top">
      <a href="<?php print $node_url; ?>">
        <?php print $title; ?>
      </a>
    </h3>

    <?php if ($display_submitted): ?>
      <div class="submitted date">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <div itemprop="description">
      <?php print render($content['body']); ?>
    </div>

    <?php
    hide($content['comments']);
    hide($content['links']);
    print render($content);
    ?>

  </div>



<?php } else { ?>



  <div class="listing-content">

    <?php if (!empty($content['field_blog_img'])) { ?>
      <?php print render($content['field_blog_img']); ?>
    <?php } ?>

    <?php if ($display_submitted): ?>
      <div class="submitted date">
        <?php print $submitted; ?>
      </div>
    <?php endif; ?>

    <div itemprop="articleBody">
      <?php print render($content['body']); ?>
    </div>

    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
    ?>

  </div>

  <?php print render($content['comments']); ?>

<?php } ?>



