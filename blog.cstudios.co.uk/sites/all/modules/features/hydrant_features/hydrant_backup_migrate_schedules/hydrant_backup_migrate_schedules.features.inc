<?php
/**
 * @file
 * hydrant_backup_migrate_schedules.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hydrant_backup_migrate_schedules_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "backup_migrate" && $api == "backup_migrate_exportables") {
    return array("version" => "1");
  }
}
