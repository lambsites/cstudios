<?php
/**
 * @file
 * hydrant_tags_and_categories.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function hydrant_tags_and_categories_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer categories vocabulary terms'.
  $permissions['administer categories vocabulary terms'] = array(
    'name' => 'administer categories vocabulary terms',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'vppr',
  );

  // Exported permission: 'administer tags vocabulary terms'.
  $permissions['administer tags vocabulary terms'] = array(
    'name' => 'administer tags vocabulary terms',
    'roles' => array(
      'Administrator' => 'Administrator',
    ),
    'module' => 'vppr',
  );

  // Exported permission: 'delete terms in categories'.
  $permissions['delete terms in categories'] = array(
    'name' => 'delete terms in categories',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in tags'.
  $permissions['delete terms in tags'] = array(
    'name' => 'delete terms in tags',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in categories'.
  $permissions['edit terms in categories'] = array(
    'name' => 'edit terms in categories',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in tags'.
  $permissions['edit terms in tags'] = array(
    'name' => 'edit terms in tags',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
