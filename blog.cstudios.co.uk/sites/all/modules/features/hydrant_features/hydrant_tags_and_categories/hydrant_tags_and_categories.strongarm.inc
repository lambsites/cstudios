<?php
/**
 * @file
 * hydrant_tags_and_categories.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function hydrant_tags_and_categories_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_categories_pattern';
  $strongarm->value = 'blog/category/[term:name]';
  $export['pathauto_taxonomy_term_categories_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_tags_pattern';
  $strongarm->value = 'blog/tag/[term:name]';
  $export['pathauto_taxonomy_term_tags_pattern'] = $strongarm;

  return $export;
}
