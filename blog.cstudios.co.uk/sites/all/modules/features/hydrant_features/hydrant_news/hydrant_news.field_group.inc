<?php
/**
 * @file
 * hydrant_news.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function hydrant_news_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basics|node|news|form';
  $field_group->group_name = 'group_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic settings',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_news_attachments',
      2 => 'field_news_img',
      3 => 'title',
      4 => 'field_attachments',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basics field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basics|node|news|form'] = $field_group;

  return $export;
}
