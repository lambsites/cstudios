<?php

/**
 * @file
 * Integration with Navbar module.
 */

/**
 * Implements hook_navbar_alter().
 */
function hydrant_settings_navbar_alter(&$items) {

  // The name of the CSS file should not interfere with other modules,
  // especially Navbar. So for example "navbar.icons.css" would break the CSS
  // files attached by to Navbar library.
  $items['administration']['#attached']['css'][] = drupal_get_path('module', 'hydrant_settings') . '/css/hydrant_settings.navbar.css';

}